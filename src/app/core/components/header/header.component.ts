import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductCategory } from '../../../shared/common/product-category';
import { ProductService } from '../../../shared/services/product.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isMenuOpen = false;
  productCategories!: ProductCategory[];

  searchForm = new UntypedFormGroup({
    search: new UntypedFormControl({ value: undefined, disabled: false }),
  })

  constructor(private productService: ProductService, private router: Router) { }

  ngOnInit(): void {
    this.loadSideMenuLinks();
  }

  toggleMenu() {
    this.isMenuOpen = !this.isMenuOpen;
  }

  loadSideMenuLinks() {
    this.productService.getProductCategories().subscribe((categories: ProductCategory[]) => {
      this.productCategories = categories;
    })
  }

  searchProduct(productName: string) {
    this.router.navigate(['/search', productName]);
  }

  pageHasHeader() {
    return  this.router.url == '/sign-up' || this.router.url == '/sign-in';
  }
}
