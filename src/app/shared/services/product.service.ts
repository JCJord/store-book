import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Product } from '../common/product';
import { ProductCategory } from '../common/product-category';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private productUrl = 'http://localhost:8080/api/products';

  private categoryUrl = 'http://localhost:8080/api/product-category'

  constructor(private httpClient: HttpClient) { }

  getProductList(): Observable<Product[]> {
    return this.httpClient.get<GetResponseProducts>(this.productUrl)
    .pipe(
      map(response => response._embedded.products)
    );
  }

  getProductCategories(): Observable<ProductCategory[]> {
    return this.httpClient.get<GetResponseProductCategories>(this.categoryUrl)
    .pipe(
      map(response => response._embedded.productCategory)
    );
  }

  getProductById(productId: number): Observable<Product> {
    return this.httpClient.get<Product>(this.productUrl + `/${productId}`);
  }

  getProductByCategoryId(categoryId: number): Observable<Product[]> {
    return this.httpClient.get<GetResponseProducts>(this.productUrl + `/search/findByCategoryId?id=${categoryId}`)
    .pipe(
      map(response => response._embedded.products)
    );
  }

  getProductByName(productName: string): Observable<Product[]> {
    return this.httpClient.get<GetResponseProducts>(this.productUrl + `/search/findByNameContaining?name=${productName}`)
    .pipe(
      map(
        response => response._embedded.products
      )
    )
  }
}

interface GetResponseProducts {
  _embedded: {
    products: Product[];
  }
}

interface GetResponseProductCategories {
  _embedded: {
    productCategory: ProductCategory[];
  }
}

