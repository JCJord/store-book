import { Injectable } from '@angular/core';
import { CartItem } from '../common/cartItem';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  cartItems: CartItem[] = [];

  constructor() { }

  addToCart(cartItem: CartItem) {
    const itemExist = this.cartItems.find((item) => item.id == cartItem.id);

    if(!itemExist){
      this.cartItems.push(cartItem);
    }
  }

  getCartItems(): CartItem[] {
    return this.cartItems;
  }
}

