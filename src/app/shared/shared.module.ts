import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ProductService } from './services/product.service';
import { TextInputComponent } from './components/input/text-input/text-input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ProductListComponent } from './components/product-list/product-list.component';

@NgModule({
  declarations: [
    TextInputComponent,
    ProductListComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  providers: [
    ProductService
  ],
  exports: [
    TextInputComponent,
    ProductListComponent,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class SharedModule { }
