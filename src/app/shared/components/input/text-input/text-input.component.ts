import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ControlContainer, FormGroupDirective, UntypedFormControl } from '@angular/forms';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss'],
  viewProviders: [
    {
        provide: ControlContainer,
        useExisting: FormGroupDirective
    }
  ]
})
export class TextInputComponent implements OnInit {

  @Output()
  enterKeyPressed = new EventEmitter<string>()

  constructor(private formGroup: FormGroupDirective) { }

  @Input()
  name!: string;

  @Input()
  type!: string;

  @Input()
  label?: string;

  @Input()
  id!: string;

  control!: UntypedFormControl;

  ngOnInit(): void {
    this.control = this.formGroup.form.get(this.name) as UntypedFormControl;
  }

  hasError(): boolean {
    return this.control.invalid && (this.control.dirty || this.control.touched);
  }

  onEnterKey() {
    this.enterKeyPressed.emit(this.control.value);
  }
}
