export class CartItem {

  constructor(public id: number, public name: string, public unitPrice: number, public imgUrl: string, public amount: number) {

  }

}
