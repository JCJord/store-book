import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Product } from 'src/app/shared/common/product';
import { ProductService } from 'src/app/shared/services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  private productSubscription$!: Subscription;
  currentCategoryId!: number  ;

  products: Product[] = [];

  constructor(private productService: ProductService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
      this.getProductsList();
    });
  }

  getProductsList() {
    this.handleProductUrl();
  }

  handleProductUrl() {
    const hasCategoryId: boolean = this.route.snapshot.paramMap.has('id');

    if(hasCategoryId){
      this.currentCategoryId = parseInt(this.route.snapshot.paramMap.get('id')!);

      this.productService.getProductByCategoryId(this.currentCategoryId).subscribe((products: Product []) => {
        this.products = products;
      });
    }else {
      this.productService.getProductList().subscribe((products: Product[]) => {
        this.products = products;
        console.log(this.products)

      })
    }
  }

}
