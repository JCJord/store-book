import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl } from '@angular/forms';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  signupForm = new UntypedFormGroup({
    name: new UntypedFormControl({ value: undefined, disabled: false }),
  })


  constructor() { }

  ngOnInit(): void {
  }

}
