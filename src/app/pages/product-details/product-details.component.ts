import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CartItem } from 'src/app/shared/common/cartItem';
import { Product } from 'src/app/shared/common/product';
import { CartService } from 'src/app/shared/services/cart.service';
import { ProductService } from 'src/app/shared/services/product.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  product!: Product;
  amountOfProducts = 1;
  isItemAdded = false;

  constructor(private productService: ProductService, private router: Router ,private activatedRoute: ActivatedRoute, private cartService: CartService) { }

  ngOnInit(): void {
    this.getProductById();
  }

  getProductById() {
    const productId = parseInt(this.activatedRoute.snapshot.paramMap.get('id')!);
    this.productService.getProductById(productId).subscribe((product: Product) => {
      this.product = product;
    })
  }

  increaseAmount(): void {
    this.amountOfProducts < 100 ? this.amountOfProducts++ : null;
  }

  decreaseAmount(): void {
    this.amountOfProducts == 1 ? null : this.amountOfProducts--;
  }

  addToCart(): void {
    const cartItem = {
      id: this.product.id,
      name: this.product.name,
      unitPrice: this.product.unitPrice,
      imgUrl: this.product.imgUrl,
      amount: this.amountOfProducts
    }

    this.cartService.addToCart(cartItem);
    this.isItemAdded = true;
  }

  redirectToCart(): void {
    this.router.navigate(['/cart']);
  }
}
