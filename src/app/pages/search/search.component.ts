import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/shared/common/product';
import { ProductService } from 'src/app/shared/services/product.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  products: Product[] = [];

  constructor(private productService: ProductService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
      this.handleSearchUrl();
    });
  }

  handleSearchUrl() {
    const hasSearchArgument: boolean = this.route.snapshot.paramMap.has('name')

    if(hasSearchArgument){
      const searchQuery = this.route.snapshot.paramMap.get('name')!;

      this.productService.getProductByName(searchQuery).subscribe((searchResult: Product[]) => {
        this.products = [...searchResult];
      });
    }
  }
}
